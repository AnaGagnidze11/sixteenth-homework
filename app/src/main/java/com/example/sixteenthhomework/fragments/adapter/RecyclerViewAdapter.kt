package com.example.sixteenthhomework.fragments.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sixteenthhomework.data.User
import com.example.sixteenthhomework.databinding.UserLayoutBinding

class RecyclerViewAdapter: RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private var userList = emptyList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(UserLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount()= userList.size


    inner class ItemViewHolder(private val binding: UserLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var user: User
        fun bind(){
            user = userList[adapterPosition]
            binding.returnedProfile.setImageResource(user.profile!!)
            binding.returnedFirstName.text = user.firstName
            binding.returnedLastName.text = user.lastName
            binding.returnedAge.text = user.age.toString()
            binding.returnedAddress.text = user.address
            binding.returnedHeight.text = user.height.toString()
        }
    }

    fun setData(users: List<User>){
        this.userList = users
        notifyDataSetChanged()
    }
}