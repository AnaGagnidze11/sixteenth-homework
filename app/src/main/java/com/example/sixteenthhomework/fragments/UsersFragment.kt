package com.example.sixteenthhomework.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sixteenthhomework.R
import com.example.sixteenthhomework.data.UserViewModel
import com.example.sixteenthhomework.databinding.FragmentUsersBinding
import com.example.sixteenthhomework.fragments.adapter.RecyclerViewAdapter

class UsersFragment : Fragment() {

    private lateinit var binding: FragmentUsersBinding

    private val viewModel: UserViewModel by viewModels()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUsersBinding.inflate(inflater, container, false)
        openAddFragment()
        initRecyclerView()
        observeData()
        return binding.root
    }

    private fun openAddFragment(){
        binding.floatingActionButton.setOnClickListener{
            findNavController().navigate(R.id.action_usersFragment_to_addUserFragment)
        }
    }

    private fun initRecyclerView(){
        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter

    }

    private fun observeData(){
        viewModel.readAllData.observe(viewLifecycleOwner ,{
            adapter.setData(it)
        })
    }

}