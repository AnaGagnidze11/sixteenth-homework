package com.example.sixteenthhomework.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sixteenthhomework.R
import com.example.sixteenthhomework.data.User
import com.example.sixteenthhomework.data.UserViewModel
import com.example.sixteenthhomework.databinding.FragmentAddUserBinding

class AddUserFragment : Fragment() {

    private lateinit var binding: FragmentAddUserBinding

    private val viewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddUserBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        binding.addUserButton.setOnClickListener {
            insertUserInDB()
        }
    }

    private fun insertUserInDB(){
        val firstName = binding.firstNameEdtTxt.text.toString()
        val lastName = binding.lastNameEdtTxt.text.toString()
        val age = binding.ageEdtTxt.text.toString()
        val address = binding.addressEdtTxt.text.toString()
        val height = binding.heightEdtTxt.text.toString()

        if (firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty() && address.isNotEmpty() && height.isNotEmpty()){
            val user = User(firstName, lastName, age.toInt(), address, height.toInt(), R.mipmap.ic_launcher)
            viewModel.addUser(user)

            Toast.makeText(requireContext(), "User has been added.", Toast.LENGTH_SHORT).show()

            findNavController().navigate(R.id.action_addUserFragment_to_usersFragment)
        }else{
            Toast.makeText(requireContext(), "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }


}