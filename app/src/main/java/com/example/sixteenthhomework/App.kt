package com.example.sixteenthhomework

import android.app.Application
import android.content.Context

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object{
        var context: Context? = null
    }
}