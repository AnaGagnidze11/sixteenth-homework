package com.example.sixteenthhomework.data

import androidx.room.Room
import com.example.sixteenthhomework.App

object DataBase {
    val db: UserDatabase by lazy {
        Room.databaseBuilder(
            App.context!!,
            UserDatabase::class.java, "users"
        ).build()
    }
}