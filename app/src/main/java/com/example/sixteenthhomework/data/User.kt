package com.example.sixteenthhomework.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_info")
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "first_name")
    val firstName: String?,
    @ColumnInfo(name = "last_name")
    val lastName: String?,
    @ColumnInfo(name = "age")
    val age: Int?,
    @ColumnInfo(name = "address")
    val address: String?,
    @ColumnInfo(name = "height")
    val height: Int?,
    @ColumnInfo(name = "profile")
    val profile: Int?
) {
    constructor(
        firstName: String?,
        lastName: String?,
        age: Int?,
        address: String?,
        height: Int?,
        profile: Int?
    ) : this(0, firstName, lastName, age, address, height, profile)
}
